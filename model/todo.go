package model

import "time"

const (
	P0         = 1 << 0
	P1         = 1 << 1
	P2         = 1 << 2
	Open       = 1 << 0
	Solve      = 1 << 1
	Delete     = 1 << 2
	CreateTime = 1 << 0
	FinishTime = 1 << 1
	Deadline   = 1 << 2
)

type Todo struct {
	Id         int
	Content    string
	Level      int //0 P0:0001, 1 P1:0010, 2 P2:0100
	Status     int //0未完成，1已完成，2删除
	CreateTime time.Time
	FinishTime time.Time
	Deadline   time.Time
}
