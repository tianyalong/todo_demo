package data

type DataOperator interface {
	Init(string) int
	ReadLine(id int) (string, error)
	Read() ([]string, error)
	Write([]string) error
	Append(line string) error
}

var Operator DataOperator

func InitOperator(fileName string) int {
	Operator = GetOperator(0)
	return Operator.Init(fileName)
}

func GetOperator(id int) DataOperator {
	switch id {
	case 0:
		return &TextOperator{}
	default:
		return &TextOperator{}
	}
}
