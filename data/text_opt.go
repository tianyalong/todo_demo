package data

import (
	"bufio"
	"errors"
	"io"
	"os"
)

type TextOperator struct {
	fileName   string
	file       *os.File
	readWriter *bufio.ReadWriter
	isCached   map[int]bool
	tmp        []string
}

func (opt *TextOperator) Init(fileName string) int {
	file, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		panic(err)
	}

	opt.isCached = make(map[int]bool)
	opt.fileName = fileName
	opt.file = file
	opt.readWriter = bufio.NewReadWriter(bufio.NewReader(file), bufio.NewWriter(file))
	lines, err := opt.Read()
	if err != nil {
		panic(err)
	}
	return len(lines)
}

func (opt *TextOperator) Read() ([]string, error) {
	opt.tmp = make([]string, 0)
	for i := 0; ; i++ {
		line, err := opt.readWriter.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		opt.tmp = append(opt.tmp, line)
		opt.isCached[i] = true
	}
	return opt.tmp, nil
}

func (opt *TextOperator) ReadLine(lineNum int) (string, error) {
	idx := lineNum - 1
	opt.tmp = make([]string, lineNum)
	for i := 0; i < lineNum; i++ {
		line, err := opt.readWriter.ReadString('\n')
		if err == io.EOF {
			return "", errors.New("no such line")
		}
		opt.tmp[i] = line[:len(line)-1]
		opt.isCached[idx] = true
	}

	return opt.tmp[idx], nil
}

func (opt *TextOperator) Write(lines []string) error {
	err := opt.file.Truncate(0)
	if err != nil {
		return err
	}
	_, err = opt.file.Seek(0, 0)
	if err != nil {
		return err
	}
	for i := 0; i < len(lines); i++ {
		_, err = opt.readWriter.WriteString(lines[i] + "\n")
		if err != nil {
			return err
		}
	}
	err = opt.readWriter.Flush()
	if err != nil {
		return err
	}
	return nil
}

func (opt *TextOperator) Append(line string) error {
	_, err := opt.readWriter.WriteString(line + "\n")
	if err != nil {
		return err
	}
	err = opt.readWriter.Flush()
	if err != nil {
		return err
	}
	return nil
}

func (opt *TextOperator) readFromCache(idx int) string {
	return opt.tmp[idx]
}

func (opt *TextOperator) hasCached(idx int) bool {
	isCached, exist := opt.isCached[idx]
	if !exist {
		return false
	}
	return isCached
}
