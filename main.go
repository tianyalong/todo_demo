package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tianyalong/todo_demo/biz"
	"gitlab.com/tianyalong/todo_demo/dao"
	"gitlab.com/tianyalong/todo_demo/data"
)

/*
todo:
	1.编写biz接口
	2.编写调用脚本
	3.编写启动、关闭todo脚本
*/

func main() {
	idx := data.InitOperator("./text_data.txt")
	dao.InitDao(idx)
	if err := InitRouter(true).Run(":8080"); err != nil {
		panic(err)
	}
}

func InitRouter(isDebug bool) *gin.Engine {
	if !isDebug {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.Default()
	r.GET("/ping", biz.Ping)
	return r
}
