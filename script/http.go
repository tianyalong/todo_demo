package main

import (
	"fmt"
	"github.com/goccy/go-json"
	"io/ioutil"
	"net/http"
	"time"
)

var (
	httpClient = &http.Client{Timeout: 60 * time.Second}
)

func doDHttpReq(req *http.Request, output interface{}) error {
	resp, err := httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	return json.Unmarshal(body, output)
}

func dail() {
	var url string = "http://0.0.0.0:8080/ping"
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("Content-Type", "application/json")
	var res map[string]interface{}
	err := doDHttpReq(req, &res)
	if err != nil {
		return
	}
	fmt.Println(res)
}
