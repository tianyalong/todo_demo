package dao

import (
	"errors"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/tianyalong/todo_demo/data"
	"gitlab.com/tianyalong/todo_demo/model"
	"time"
)

type GeneralDao struct {
	nextIdx int
}

func (dao *GeneralDao) Init(idx int) {
	dao.nextIdx = idx
}

func (dao *GeneralDao) NextIdx() int {
	dao.nextIdx++
	return dao.nextIdx
}

func (dao *GeneralDao) Create(content string, level int, deadline time.Time) error {
	var todo = model.Todo{
		Id:         dao.NextIdx(),
		Content:    content,
		Level:      level,
		Status:     0,
		CreateTime: time.Now(),
		FinishTime: time.Time{},
		Deadline:   time.Time{},
	}
	todoByte, err := jsoniter.Marshal(todo)
	if err != nil {
		return err
	}
	err = data.Operator.Append(string(todoByte))
	if err != nil {
		return err
	}
	return nil
}

func (dao *GeneralDao) Finish(id int) error {
	return dao.changeStatus(id, 1)
}

func (dao *GeneralDao) Delete(id int) error {
	return dao.changeStatus(id, 2)
}

func (dao *GeneralDao) changeStatus(id, status int) error {
	lines, err := data.Operator.Read()
	if err != nil {
		return err
	}
	for i, line := range lines {
		var todo model.Todo
		err := jsoniter.Unmarshal([]byte(line), &todo)
		if err != nil {
			return err
		}
		if todo.Id == id {
			todo.Status = status
			todo.FinishTime = time.Now()
		}
		lineByte, _ := jsoniter.Marshal(todo)
		lines[i] = string(lineByte)
		err = data.Operator.Write(lines)
		if err != nil {
			return err
		}
		return nil
	}
	return errors.New("no such id")
}

func (dao *GeneralDao) TodoList(level, status int, timeType int, stTime time.Time, edTime time.Time) ([]model.Todo, error) {
	todos, err := dao.getTodos()
	if err != nil {
		return nil, err
	}
	todos = dao.filterLevel(todos, level)
	todos = dao.filterStatus(todos, status)
	todos = dao.filterTime(todos, timeType, stTime, edTime)
	return todos, nil
}

func (dao *GeneralDao) getTodos() ([]model.Todo, error) {
	var todos []model.Todo
	lines, err := data.Operator.Read()
	if err != nil {
		return nil, err
	}
	for _, line := range lines {
		var todo model.Todo
		err = jsoniter.Unmarshal([]byte(line), &todo)
		if err != nil {
			return nil, err
		}
		todos = append(todos, todo)
	}
	return todos, nil
}

func (dao *GeneralDao) filterLevel(todos []model.Todo, level int) []model.Todo {
	var tmp []model.Todo
	for i := 0; i < len(todos); i++ {
		if todos[i].Level&level == 0 {
			continue
		}
		tmp = append(tmp, todos[i])
	}
	return tmp
}

func (dao *GeneralDao) filterStatus(todos []model.Todo, status int) []model.Todo {
	var tmp []model.Todo
	for i := 0; i < len(todos); i++ {
		if todos[i].Status&status == 0 {
			continue
		}
		tmp = append(tmp, todos[i])
	}
	return tmp
}

func (dao *GeneralDao) filterTime(todos []model.Todo, timeType int, stTime, edTime time.Time) []model.Todo {
	var tmp []model.Todo
	for i := 0; i < len(todos); i++ {
		if timeType&model.CreateTime != 0 && (todos[i].CreateTime.Before(stTime) || todos[i].CreateTime.After(edTime)) {
			continue
		}
		if timeType&model.FinishTime != 0 && (todos[i].FinishTime.Before(stTime) || todos[i].FinishTime.After(edTime)) {
			continue
		}
		if timeType&model.Deadline != 0 && (todos[i].Deadline.Before(stTime) || todos[i].Deadline.After(edTime)) {
			continue
		}
		tmp = append(tmp, todos[i])
	}
	return tmp
}
