package dao

import (
	"gitlab.com/tianyalong/todo_demo/model"
	"time"
)

var Dao TodoDao

type TodoDao interface {
	Init(int)
	Create(content string, level int, deadline time.Time) error
	Finish(id int) error
	Delete(id int) error
	TodoList(level int, status int, timeType int, stTime time.Time, edTime time.Time) ([]model.Todo, error)
}

func InitDao(idx int) {
	Dao = GetDao(0)
	Dao.Init(idx)
}

func GetDao(id int) TodoDao {
	switch id {
	case 0:
		return &GeneralDao{}
	default:
		return &GeneralDao{}
	}
}
